#include <stdint.h>
#include <stdio.h>
#include <QImage>
#include <QImageIOHandler>

extern "C" {
#include <libbpg.h>
}

#include "bpghandler.h"

class BpgHandler: public QImageIOHandler {
public:
  BpgHandler() {}
  ~BpgHandler() {
      if (img) {
          bpg_decoder_close(img);
          img = nullptr;
      }
  }

  bool canRead() const { return canRead(device()); }
  static bool canRead( QIODevice *device );

  QVariant option(QImageIOHandler::ImageOption option) const override {
      if (!ensureInitialized()) {
          return QVariant();
      }
      switch(option) {
      case Size:
          return QSize(img_info.width, img_info.height);
      default:
          return QVariant();
      }
  }
  bool supportsOption(QImageIOHandler::ImageOption option) const override {
      switch(option) {
      case Size:
          return true;
      default:
          return false;
      }
  }
  bool read( QImage *image );
  QByteArray name() const { return "bpg"; }

private:
  bool ensureInitialized() const {
      if (m_failed) {
          return false;
      }
      if (img) {
          return true;
      }

      BpgHandler *that = const_cast<BpgHandler*>(this);
      that->img = bpg_decoder_open();
      if (!img) {
          that->m_failed = true;
          return false;
      }

      qint64 pos = device()->pos();
      QByteArray buf = device()->readAll();
      device()->seek(pos);

      if (bpg_decoder_decode(img, (uint8_t*)buf.data(), buf.size()) < 0) {
          that->m_failed = true;
          return false;
      }
      int ret = bpg_decoder_get_info(that->img, &that->img_info);
      if (ret < 0) {
          that->m_failed = true;
          return false;
      }

      return true;
  }
  bool m_failed = false;
  BPGDecoderContext *img = nullptr;
  BPGImageInfo img_info;
};

bool BpgHandler::canRead( QIODevice *device ) {
  return device->peek(3) == "BPG";
}

static inline uint32_t bgr_to_rgb( uint32_t rgb ) {
  return ((rgb << 16) & 0xff0000) | ((rgb >> 16) & 0xff) | (rgb & 0xff00ff00);
}

bool BpgHandler::read( QImage *image ) {
  if (!ensureInitialized()) {
    return false;
  }
  int w, h;
  bool ret = true;

  w = img_info.width;
  h = img_info.height;

  *image = *new QImage( w, h, QImage::Format_ARGB32 );

  bpg_decoder_start(img, BPG_OUTPUT_FORMAT_RGBA32);

  for (int y = 0; y < h; ++y) {
  	uint32_t *scanLine = (uint32_t*)image->scanLine(y);
	bpg_decoder_get_line(img, scanLine);
	for ( int x = 0; x < w; x++ )
		scanLine[x] = bgr_to_rgb( scanLine[x] );
  }

  return ret;
}

QImageIOPlugin::Capabilities BpgPlugin::capabilities( QIODevice *device, const QByteArray &format ) const {

  if ( format == "bpg" ) return Capabilities(CanRead);
  if ( !format.isEmpty() || !device->isOpen() ) return Capabilities();

  if ( device->isReadable() && BpgHandler::canRead(device) )
  	return Capabilities(CanRead);

  return Capabilities();
}

QImageIOHandler *BpgPlugin::create( QIODevice *device, const QByteArray &format ) const {
  BpgHandler *handler = new BpgHandler();
  handler->setDevice( device );
  handler->setFormat( format );
  return handler;
}

