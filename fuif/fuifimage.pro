TEMPLATE = lib
CONFIG  += qt plugin
TARGET   = fuif
SOURCES += fuifhandler.cpp
HEADERS += fuifhandler.h
CONFIG += optimize_full

INCLUDEPATH += libfuif
LIBS        += -L$$(FUIFLIB_PATH) -lfuif

# libfuif (which is dead, so bundled here)

SOURCES += \
    libfuif/image/image.cpp \
    libfuif/transform/transform.cpp \
    libfuif/maniac/bit.cpp \
    libfuif/maniac/chance.cpp \
    libfuif/encoding/encoding.cpp \
    libfuif/io.cpp
