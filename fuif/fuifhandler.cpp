#include <stdint.h>
#include <stdio.h>
#include <QImage>
#include <QImageIOHandler>
#include <QElapsedTimer>

#include <memory>

#include <encoding/encoding.h>

#include "fuifhandler.h"

static const bool benchmark = qEnvironmentVariableIsSet("QT_FUIF_BENCH");

class FuifHandler: public QImageIOHandler {
public:
  FuifHandler() {}
  ~FuifHandler() {
      //if (f) {
      //    fclose(f);
      //}
  }

  bool canRead() const { return canRead(device()); }
  static bool canRead( QIODevice *device );

  bool read( QImage *image );
  QByteArray name() const { return "fuif"; }

  QVariant option(QImageIOHandler::ImageOption option) const override {
      if (!ensureInitialized(false)) {
          return QVariant();
      }
      switch(option) {
      case Size:
          return QSize(w, h);
      default:
          return QVariant();
      }
  }
  bool supportsOption(QImageIOHandler::ImageOption option) const override {
      switch(option) {
      case Size:
          return true;
      default:
          return false;
      }
  }

private:
  bool ensureInitialized(bool imageData) const {
      if (m_failed) {
          return false;
      }
      if (!imageData) {
          if (w > 0 && h > 0) {
              return true;
          }
      } else {
          if (m_initialized) {
              return true;
          }
      }

      return const_cast<FuifHandler*>(this)->initialize(imageData);
  }

  bool initialize(bool imageData) {
      QElapsedTimer t; t.start();
      if (benchmark) qDebug() << "Reading image, all?" << imageData;
      qint64 pos = device()->pos();
      buf = device()->readAll();
      device()->seek(pos);
      qDebug() << buf.mid(0, 5);
      if (!buf.startsWith("FUIF") && !buf.startsWith("FUAF")) {
          qWarning() << "Invalid FUIF image file";
          m_failed = true;
          return false;
      }

      if (fio) {
          qDebug() << "Resetting fio";
          fio.reset();
          f = nullptr;
      }

      FILE *f = fmemopen( buf.data(), buf.size(), "rb" );
      if (!f) {
          m_failed = true;
          return false;
      }
      fio = std::make_unique<FileIO>( f, "<filename>" );

      fuif_options options = default_fuif_options;
      if (!imageData) {
          options.identify = true;
      }

      if (!fuif_decode(*fio, img, options)) {
          m_failed = true;
          return false;
      }

      if (imageData) {
          img.undo_transforms();
      } else {
          fio.reset();
          f = nullptr;
      }

      w = img.w;
      h = img.h / img.nb_frames;
      if (benchmark) qDebug() << "init completed in" << t.elapsed();

      return true;
  }

  bool m_initialized = false;
  bool m_failed = false;
  QByteArray buf;
  FILE *f = nullptr;

  std::unique_ptr<FileIO> fio;
  Image img;

  int w = 0, h = 0;
};

bool FuifHandler::canRead( QIODevice *device ) {
  return device->peek(4) == "FUIF";
}

bool FuifHandler::read( QImage *image ) {
    if (!ensureInitialized(true)) {
        return false;
    }


  if (img.nb_channels == 3) {
      *image = QImage( w, h, QImage::Format_RGB888 );
  } else if (img.nb_channels == 4) {
      *image = QImage( w, h, QImage::Format_ARGB32 );
  } else {
      qWarning() << "Unsupported number of channels" << img.nb_channels;
      m_failed = true;
  }

  std::vector<uint8_t> buffer_(w);
  uint8_t *buffer = buffer_.data();

  for (int channel=0; channel<img.nb_channels; channel++) {
      pixel_type *src = img.channel[channel].rawData;
      const int channelWidth = img.channel[channel].w;
      for (int y = 0; y < h; ++y) {
          for ( int x = 0; x < w; x++ ) {
              buffer[x] = src[y * channelWidth + x];
          }

          uchar *dst = image->scanLine(y);
          if (img.nb_channels == 3) {
              for ( int x = 0; x < w; x++ ) {
                  dst[x * 3 + channel] = buffer[x];
              }
          } else {
              for ( int x = 0; x < w; x++ ) {
                  dst[x * 4 + channel] = buffer[x];
              }
          }
      }
  }

  return true;
}

QImageIOPlugin::Capabilities FuifPlugin::capabilities( QIODevice *device, const QByteArray &format ) const {

  if ( format == "fuif" ) return Capabilities(CanRead);
  if ( !format.isEmpty() || !device->isOpen() ) return Capabilities();

  if ( device->isReadable() && FuifHandler::canRead(device) )
  	return Capabilities(CanRead);

  return Capabilities();
}

QImageIOHandler *FuifPlugin::create( QIODevice *device, const QByteArray &format ) const {
  FuifHandler *handler = new FuifHandler();
  handler->setDevice( device );
  handler->setFormat( format );
  return handler;
}
