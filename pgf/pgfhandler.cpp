#include <stdint.h>
#include <stdio.h>
#include <QImage>
#include <QImageIOHandler>

#include <memory>

#include <libpgf/PGFimage.h>

#include "pgfhandler.h"

class PgfHandler: public QImageIOHandler {
public:
  PgfHandler() {}

  bool canRead() const { return canRead(device()); }
  static bool canRead( QIODevice *device );

  bool read( QImage *image );
  QByteArray name() const { return "pgf"; }

  QVariant option(QImageIOHandler::ImageOption option) const override {
    if (!ensureInitialized()) {
      return QVariant();
    }
    switch(option) {
    case Size:
      return QSize(w, h);
    default:
      return QVariant();
    }
  }

  bool supportsOption(QImageIOHandler::ImageOption option) const override {
    switch(option) {
    case Size:
      return true;
    default:
      return false;
    }
  }
private:
  bool ensureInitialized() const {
    if (m_failed) {
      return false;
    }
    if (pgfMS) {
      return true;
    }

    PgfHandler *that = const_cast<PgfHandler*>(this);

    const qint64 pos = device()->pos();
    that->buf = device()->readAll();
    device()->seek(pos);

    that->pgfMS = std::make_unique<CPGFMemoryStream>( (unsigned char*)that->buf.data(), buf.size() );

    try {
        that->pgf.Open( that->pgfMS.get() );
        that->pgf.Read() ;
    }
    catch (IOException&) {
        puts("PGFHandler read error");
        that->m_failed = true;
        that->pgfMS.reset();
        return false;
    }

    that->w = pgf.Width();
    that->h = pgf.Height();
    return true;
  }

  int w = 0, h = 0;
  bool m_failed = false;

  std::unique_ptr<CPGFMemoryStream> pgfMS;
  CPGFImage pgf;
  QByteArray buf;
};

bool PgfHandler::canRead( QIODevice *device ) {
  return device->peek(3) == "PGF";
}

bool PgfHandler::read( QImage *image ) {
  if (!ensureInitialized()) {
    return false;
  }
  bool ret = true;

  *image = *new QImage( w, h, QImage::Format_RGB888 );

  try {
	int map[] = { 2, 1, 0 };
	pgf.GetBitmap( image->bytesPerLine(), image->bits(), 24, map );
  }
  catch (IOException&) {
	delete image;
	*image = QImage();

	puts("PGFHandler decode error");
	return false;
  }

  return ret;
}

QImageIOPlugin::Capabilities PgfPlugin::capabilities( QIODevice *device, const QByteArray &format ) const {
  if ( format == "pgf" ) return Capabilities(CanRead);
  if ( !format.isEmpty() || !device->isOpen() ) return 0;

  if ( device->isReadable() && PgfHandler::canRead(device) )
  	return Capabilities(CanRead);

  return 0;
}

QImageIOHandler *PgfPlugin::create( QIODevice *device, const QByteArray &format ) const {
  PgfHandler *handler = new PgfHandler();
  handler->setDevice( device );
  handler->setFormat( format );
  return handler;
}
